// 工厂模式
function Person(name, age) {
    var obj = new Object();
    obj.name = name;
    obj.age = age;
    return obj;
}
Person.prototype.say = function() {
    console.log(this.name);
}
var p = new Person('张三丰',100);
var p = Person('张三丰', 100);
